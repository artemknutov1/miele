// owl carpusel
$(document).ready(function() {
	let owl = $('.owl-carousel');
	owl.owlCarousel({
		margin: 15,
		items: 1,
		navText: ['',''],
		nav: true,
		loop: true,
		responsive: {}
	});
	$(".next").click(function(){
		owl.trigger("next.owl.carousel");
	});
	$(".prev").click(function(){
		owl.trigger("prev.owl.carousel");
	});
});

// Random id
function str_rand() {
	var result       = '';
	var words        = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
	var max_position = words.length - 1;
	for( i = 0; i < 5; ++i ) {
		position = Math.floor ( Math.random() * max_position );
		result = result + words.substring(position, position + 1);
	}
	const inputId = document.querySelector('.id');
	inputId.value = result;
	return result;
}

const agreeCb= document.querySelector('#agree_cb');

agreeCb.addEventListener('click', () => {
	str_rand();
});




